" Vim Config                                                                                                                                                                                                                                

" Plugins
call plug#begin('~/.vim/plugged')

" Goyo
Plug 'junegunn/goyo.vim'

" Onehalf Theme
Plug 'sonph/onehalf', {'rtp': 'vim/'}

" Airline
Plug 'vim-airline/vim-airline'

" Markdown
Plug 'godlygeek/tabular'

Plug 'plasticboy/vim-markdown'

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

" Latex
Plug 'lervag/vimtex'

" Initialize plugin system
call plug#end()

" Theming Related
set wildmenu					" Display all matches when tab complete.
set incsearch                   " Incremental search
set hidden                      " Needed to keep multiple buffers open
set nobackup                    " No auto backups
set noswapfile                  " No swap
set t_Co=256                    " Set if term supports 256 colors.
set number relativenumber       " Display line numbers
set clipboard=unnamedplus       " Copy/paste between vim and other programs.
syntax enable
colorscheme onehalfdark
let g:airline_theme='onehalfdark'

if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" Airline Config
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline#extensions#tabline#left_sep = '>'
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_left_sep='>'
let g:airline_right_sep='<'
map ** :! pdflatex %<CR><CR>
map ^^ :! zathura $(echo % \| sed 's/tex$/pdf/') & disown<CR><CR>
