#
# ~/.bashrc
#

# Aliases

alias ls='ls -l --color=auto'
alias la='ls -lav --ignore=..'   # show long listing of all except ".."
alias l='ls -lav --ignore=.?*'   # show long listing but no hidden dotfiles except "."
alias paci='sudo pacman -S'
alias pacu='sudo pacman -Syu'
alias pacr='sudo pacman -R'
alias yayi='yay -S'
alias yayu='yay -Syu'
alias yayr='yay -R'
alias yayq='yay -Q'
alias yays='yay -Syy'
alias i3conf='vim ~/.config/i3/config'
alias neofetch='neofetch --ascii_distro Arch'
alias cmatrix='cmatrix' 
alias keypress='sh ~/.scripts/keyboarddetect.sh'
alias config='/usr/bin/git --git-dir=/home/bhud/dotfiles --work-tree=/home/bhud'
#alias su='su -'
export PAGER=/usr/local/bin/vimpager
alias less=$PAGER
alias zless=$PAGER


[[ "$(whoami)" = "root" ]] && return

[[ -z "$FUNCNEST" ]] && export FUNCNEST=100          # limits recursive functions, see 'man bash'

## Use the up and down arrow keys for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

################################################################################
## Some generally useful functions.
## Consider uncommenting aliases below to start using these functions.


_GeneralCmdCheck() {
    # A helper for functions UpdateArchPackages and UpdateAURPackages.

    echo "$@" >&2
    "$@" || {
        echo "Error: '$*' failed." >&2
        exit 1
    }
}

_CheckInternetConnection() {
    curl --silent --connect-timeout 8 https://8.8.8.8 >/dev/null
    local result=$?
    test $result -eq 0 || echo "No internet connection!" >&2
    return $result
}

_CheckArchNews() {
    local conf=/etc/eos-update-notifier.conf

    if [ -z "$CheckArchNewsForYou" ] && [ -r $conf ] ; then
        source $conf
    fi

    if [ "$CheckArchNewsForYou" = "yes" ] ; then
        local news="$(yay -Pw)"
        if [ -n "$news" ] ; then
            echo "Arch news:" >&2
            echo "$news" >&2
            echo "" >&2
            # read -p "Press ENTER to continue (or Ctrl-C to stop): "
        else
            echo "No Arch news." >&2
        fi
    fi
}

UpdateArchPackages() {
    # Updates Arch packages.

    _CheckInternetConnection || return 1

    _CheckArchNews

    #local updates="$(yay -Qu --repo)"
    local updates="$(checkupdates)"
    if [ -n "$updates" ] ; then
        echo "Updates from upstream:" >&2
        echo "$updates" | sed 's|^|    |' >&2
        _GeneralCmdCheck sudo pacman -Syu "$@"
        return 0
    else
        echo "No upstream updates." >&2
        return 1
    fi
}

UpdateAURPackages() {
    # Updates AUR packages.

    _CheckInternetConnection || return 1

    local updates
    if [ -x /usr/bin/yay ] ; then
        updates="$(yay -Qua)"
        if [ -n "$updates" ] ; then
            echo "Updates from AUR:" >&2
            echo "$updates" | sed 's|^|    |' >&2
            _GeneralCmdCheck yay -Syua "$@"
        else
            echo "No AUR updates." >&2
        fi
    else
        echo "Warning: /usr/bin/yay does not exist." >&2
    fi
}

UpdateAllPackages() {
    # Updates all packages in the system.
    # Upstream (i.e. Arch) packages are updated first.
    # If there are Arch updates, you should run
    # this function a second time to update
    # the AUR packages too.

    UpdateArchPackages || UpdateAURPackages
}


_open_files_for_editing() {
    # Open any given document file(s) for editing (or just viewing).
    # Note1: Do not use for executable files!
    # Note2: uses mime bindings, so you may need to use
    #        e.g. a file manager to make some file bindings.

    local progs="xdg-open exo-open"     # One of these programs is used.
    local prog
    for prog in $progs ; do
        if [ -x /usr/bin/$xx ] ; then
            $prog "$@" >& /dev/null &
            return
        fi
    done
    echo "Sorry, none of programs [$progs] is found." >&2
    echo "Tip: install one of packages" >&2
    for prog in $progs ; do
        echo "    $(pacman -Qqo "$prog")" >&2
    done
}

neofetch --ascii_distro Arch ;

################################################################################

#
# Custom Prompt --------------------------------------------------------------------------------------------
#

# Defining Colors

COL0='\e[38;5;0m'
COL1='\e[38;5;1m'
COL2='\e[38;5;2m'
COL3='\e[38;5;3m'
COL4='\e[38;5;4m'
COL5='\e[38;5;5m'
COL6='\e[38;5;6m'
COL7='\e[38;5;7m'

# Unicode Characters
# ╭ ╰ ─ ⮞ ⸺

# ╭───<User>⸺   <Dir>
# ╰─➤

# If user is root, red prompt, otherwise green

if (( UID == 0)); then
    PS1="${COL1}╭───\
${COL7}\u${COL1}⸺  \
 ${COL4}\w \
\n${COL1}╰─➤ ${COL7}"
else
    PS1="${COL2}╭───\
${COL7}\u${COL2}⸺  \
 ${COL4}\w \
\n${COL2}╰─➤ ${COL7}"  

fi

# ----------------------------------------------------------------------------------------------------------

